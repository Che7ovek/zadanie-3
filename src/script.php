//title: Замена 0 на null

use Bitrix\Main\Application,
	Bitrix\Main\IO;

$originalFilePath = Application::getDocumentRoot() . '/' . \Bitrix\Main\Config\Option::get('main', 'upload_dir') . '/chart.json';
$resultFilePath = Application::getDocumentRoot() . '/' . \Bitrix\Main\Config\Option::get('main', 'upload_dir') . '/chart_result.json';
$file =  new IO\File($originalFilePath);

if($file->isExists())
{
	$content = $file->getContents();
	$arChart = json_decode($content);
	$arNulledColumn = [];
	if($arChart !== null) {
		// Просто проверим первые 4 элемента в столбце, если там везде 100, то это наш пациент
		for($i = 0; $i < 4; $i++)
		{
			for($j = 1; $j < count($arChart[$i]); $j++)
			{
				if($arChart[$i][$j] === 100 && $arNulledColumn[$j] !== false) // Если в этом столбце продолжаются 100, то нам нужен его номер
					$arNulledColumn[$j] = $j;
				else // Если нет, то всё, ставим флаг, как необнуляемый столбец
					$arNulledColumn[$j] = false;
			}
		}
		// Оставляем только те номера столбцов, которые надо обnullять (КАЛАМБУРЫ ЭТО МОЯ ЖИЗНЬ)
		foreach($arNulledColumn as $index => $isValue)
		{
			if(!$isValue)
				unset($arNulledColumn[$index]);
		}
		// Идём обнулять значения
		foreach($arChart as $rowIndex => $row)
		{
			foreach($arNulledColumn as $cIndex => $column)
			{
				if($row[$column] === 100) // Если 100, то обнуляем
					$arChart[$rowIndex][$column] = null;
				else // Если не 100, то перестаём обнулять этот столбец
					unset($arNulledColumn[$cIndex]);
			}
			// Когда закончатся столбцы для обнуления, заканчиваем
			if(empty($arNulledColumn))
				break;
		}

		// Пишем в файл
		$resultContent = json_encode($arChart);
		$resultFile =  new IO\File($resultFilePath);
		$resultFile->putContents($resultContent);
	}
}